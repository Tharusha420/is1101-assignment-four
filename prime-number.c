#include <stdio.h>
int main() {
  int x, i, flag = 0;
  printf("Enter a number \n");
  scanf("%d", &x);

  for (i = 2; i<x ; ++i) {
    if (x % i == 0) {
      flag = 1;
      break;
    }
  }

  if (x == 1) {
    printf("1 is not a prime number or a composite number");
  }
  else {
    if (flag == 1)
      printf("%d is not a prime number.", x);
    else
      printf("%d is a prime number.", x);
  }

  return 0;
}




